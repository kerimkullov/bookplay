import 'package:bookPlay/screens/admin/admin_auth.dart';
import 'package:bookPlay/screens/admin/admin_login.dart';
import 'package:bookPlay/screens/admin/chapter_info.dart';
import 'package:bookPlay/screens/admin/chapters.dart';
import 'package:bookPlay/screens/select_user_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: "/",
      routes: {
        "/": (context) => SelectUserScreen(),
        "/admin_login": (context) => AdminLogin(),
        "/admin_auth": (context) => AdminAuth(),
        "/admin_login/chapters": (context) => ChapterScreen(),
        "/admin_login/chapter_info": (context) => ChapterInfoScreen(),
        // "/": (context) => SelectUserScreen(),
      },
    );
  }
}
