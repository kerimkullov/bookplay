import 'package:flutter/material.dart';

class BookColors {
  static Color background = Color(0xffE5E5E5);
  static Color circle = Color(0xffC4C4C4);
}
