import 'package:flutter/material.dart';

class CustomDropDonw extends StatelessWidget {
  final List<String> list;
  const CustomDropDonw({Key key, this.list}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: DropdownButton<String>(
        icon: Icon(Icons.keyboard_arrow_down),
        isExpanded: true,
        value: list[0],
        items: list.map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
        onChanged: (_) {},
      ),
    );
  }
}
