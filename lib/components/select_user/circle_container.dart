import 'package:bookPlay/contants/book_colors.dart';
import 'package:flutter/material.dart';

class CircleContainer extends StatelessWidget {
  final String toScreen;
  final String title;
  const CircleContainer({Key key, this.title, this.toScreen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.pushNamed(context, toScreen),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: BookColors.circle, borderRadius: BorderRadius.circular(50)),
        height: 100,
        width: 100,
        child: Text(
          title,
        ),
      ),
    );
  }
}
