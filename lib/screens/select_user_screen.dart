import 'package:bookPlay/components/select_user/circle_container.dart';
import 'package:bookPlay/contants/book_colors.dart';
import 'package:flutter/material.dart';

class SelectUserScreen extends StatelessWidget {
  const SelectUserScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BookColors.background,
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            CircleContainer(
              toScreen: "/admin_login",
              title: "Admin",
            ),
            SizedBox(
              height: 50,
            ),
            CircleContainer(
              toScreen: "user",
              title: "User",
            ),
          ],
        ),
      ),
    );
  }
}
