import 'package:flutter/material.dart';

class ChapterScreen extends StatelessWidget {
  const ChapterScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chapters"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (context, i) => Container(
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: ListTile(
                                contentPadding: EdgeInsets.all(16),
                                title: Text("list $i"),
                              ),
                            ),
                          ],
                        ),
                      )),
              SizedBox(height: 30),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(
                    context, "/admin_login/chapter_info",
                    arguments: {"isAdd": true}),
                child: Text(
                  "Add",
                ),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 42),
                    primary: Colors.grey),
              ),
              SizedBox(height: 30),
              ElevatedButton(
                onPressed: () => Navigator.pushNamed(
                    context, "/admin_login/chapter_info",
                    arguments: {"isAdd": false}),
                child: Text(
                  "Logout",
                ),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 32),
                    primary: Colors.grey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
