import 'package:flutter/material.dart';

class AdminAuth extends StatelessWidget {
  const AdminAuth({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Admin Auth"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Login",
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(height: 10),
              TextField(
                decoration: InputDecoration(
                    hintText: "Login", border: OutlineInputBorder()),
              ),
              SizedBox(height: 30),
              Text(
                "Title",
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(height: 10),
              TextField(
                decoration: InputDecoration(
                    hintText: "Title", border: OutlineInputBorder()),
              ),
              SizedBox(height: 30),
              ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, "/admin_login/chapters"),
                child: Text(
                  "Login",
                ),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 38),
                    primary: Colors.grey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
