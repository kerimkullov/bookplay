import 'package:bookPlay/components/custom_dropdown.dart';
import 'package:flutter/material.dart';

class ChapterInfoScreen extends StatelessWidget {
  const ChapterInfoScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(
        title: Text(args["isAdd"] ? "Add Chapter" : "Chapters"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (context, i) => CustomDropDonw(
                        list: ['A', 'B', 'C', 'D'],
                      )),
              SizedBox(height: 30),
              ElevatedButton(
                onPressed: () =>
                    Navigator.pushNamed(context, "/admin_login/chapters"),
                child: Text(
                  "Add",
                ),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 42),
                    primary: Colors.grey),
              ),
              SizedBox(height: 30),
              ElevatedButton(
                onPressed: () {},
                child: Text(
                  "Logout",
                ),
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 32),
                    primary: Colors.grey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
